import logo from "./logo.svg";
import "./App.css";
import Shoe_Shop_Hook from "./Shoe_Shop_Hook/Shoe_Shop_Hook";

function App() {
  return (
    <div className="App">
      <Shoe_Shop_Hook />
    </div>
  );
}

export default App;

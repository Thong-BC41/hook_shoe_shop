import React from "react";
import ItemShoe from "./ItemShoe";

function ListShoe({ list, handleAddToCart }) {
  return (
    <div className="row">
      {list.map((item) => {
        return <ItemShoe shoe={item} handleAddToCart={handleAddToCart} />;
      })}
    </div>
  );
}

export default ListShoe;

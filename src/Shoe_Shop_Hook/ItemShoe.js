import React from "react";

function ItemShoe({ shoe, handleAddToCart }) {
  return (
    <div className="card col-3 my-1">
      <img className="card-img-top" src={shoe.image} alt="" />
      <div className="card-body">
        <h2 className="card-title">{shoe.name}</h2>
        <p className="card-text">{shoe.price}</p>
        <button
          onClick={() => {
            handleAddToCart(shoe);
          }}
          className="btn btn-success"
        >
          Add to cart
        </button>
      </div>
    </div>
  );
}
export default ItemShoe;

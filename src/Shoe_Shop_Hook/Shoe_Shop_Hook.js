import React, { useState } from "react";
import CartShoe from "./CartShoe";
import { data_shoe } from "./data_Shoe";
import ListShoe from "./ListShoe";

function Shoe_Shop_Hook() {
  const [list] = useState(data_shoe);
  const [ShoeCart, setShoeCart] = useState([]);

  let handleAddToCart = (shoe) => {
    let cloneCart = [...ShoeCart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soluong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soluong++;
    }
    setShoeCart(cloneCart);
  };
  let handleChangeQuantity = (idShoe, tanggiam) => {
    let cloneCart = [...ShoeCart];
    let index = cloneCart.findIndex((item) => {
      return item.id == idShoe;
    });
    cloneCart[index].soluong = cloneCart[index].soluong + tanggiam;
    if (cloneCart[index].soluong >= 1) {
      setShoeCart(cloneCart);
    } else {
      let newCart = ShoeCart.filter((item) => {
        return item.id != idShoe;
      });
      setShoeCart(newCart);
    }
  };

  let handleDelete = (idShoe) => {
    let newCart = ShoeCart.filter((item) => {
      return item.id != idShoe;
    });
    setShoeCart(newCart);
  };

  return (
    <div className="container">
      <CartShoe
        cart={ShoeCart}
        handleChangeQuantity={handleChangeQuantity}
        handleDelete={handleDelete}
      />
      <ListShoe list={list} handleAddToCart={handleAddToCart} />
    </div>
  );
}

export default Shoe_Shop_Hook;
